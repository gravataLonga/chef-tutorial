package 'apache2'

service 'apache2' do
  supports :status => true
  action [:enable, :start]
end

file '/var/www/html/index.html' do
	content '<html>
			<title>Hello World :: Jonathan</title>
			<body>
				<h1>Hello Jonathan Fontes</h1>
			</body>
		</html>'
end
