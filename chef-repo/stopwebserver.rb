service 'apache2' do
	action [:disable, :stop]
end

package 'apache2' do
	action :purge
end
